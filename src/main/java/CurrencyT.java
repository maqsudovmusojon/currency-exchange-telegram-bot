public class CurrencyT {
    private float id;
    private String Code;
    private String Ccy;
    private String CcyNm_RU;
    private String CcyNm_UZ;
    private String CcyNm_UZC;
    private String CcyNm_EN;
    private String Nominal;
    private String Rate;
    private String Diff;
    private String Date;


    // Getter Methods

    public float getId() {
        return id;
    }

    public String getCode() {
        return Code;
    }

    public String getCcy() {
        return Ccy;
    }

    public String getCcyNm_RU() {
        return CcyNm_RU;
    }

    public String getCcyNm_UZ() {
        return CcyNm_UZ;
    }

    public String getCcyNm_UZC() {
        return CcyNm_UZC;
    }

    public String getCcyNm_EN() {
        return CcyNm_EN;
    }

    public String getNominal() {
        return Nominal;
    }

    public String getRate() {
        return Rate;
    }

    public String getDiff() {
        return Diff;
    }

    public String getDate() {
        return Date;
    }

    // Setter Methods

    public void setId(float id) {
        this.id = id;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public void setCcy(String Ccy) {
        this.Ccy = Ccy;
    }

    public void setCcyNm_RU(String CcyNm_RU) {
        this.CcyNm_RU = CcyNm_RU;
    }

    public void setCcyNm_UZ(String CcyNm_UZ) {
        this.CcyNm_UZ = CcyNm_UZ;
    }

    public void setCcyNm_UZC(String CcyNm_UZC) {
        this.CcyNm_UZC = CcyNm_UZC;
    }

    public void setCcyNm_EN(String CcyNm_EN) {
        this.CcyNm_EN = CcyNm_EN;
    }

    public void setNominal(String Nominal) {
        this.Nominal = Nominal;
    }

    public void setRate(String Rate) {
        this.Rate = Rate;
    }

    public void setDiff(String Diff) {
        this.Diff = Diff;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "id=" + id +
                ", Code='" + Code + '\'' +
                ", Ccy='" + Ccy + '\'' +
                ", CcyNm_RU='" + CcyNm_RU + '\'' +
                ", CcyNm_UZ='" + CcyNm_UZ + '\'' +
                ", CcyNm_UZC='" + CcyNm_UZC + '\'' +
                ", CcyNm_EN='" + CcyNm_EN + '\'' +
                ", Nominal='" + Nominal + '\'' +
                ", Rate='" + Rate + '\'' +
                ", Diff='" + Diff + '\'' +
                ", Date='" + Date + '\'' +
                '}';
    }
}
