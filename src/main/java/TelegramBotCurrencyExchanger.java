import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//Here we have connected to Telegram API
public class TelegramBotCurrencyExchanger extends TelegramLongPollingBot {
    int counterUZS_USD=0;
    int counterUZS_EUR=0;
    int counterUZS_CYN=0;
    int counterUSD_UZS=0;
    int counterEUR_UZS=0;
    int counterCYN_UZS=0;

    //This  is driver class
    public static void main(String[] args) {
        ApiContextInitializer.init(); // Here we have initialized Api
        TelegramBotsApi api = new TelegramBotsApi();

        try {
            api.registerBot(new TelegramBotCurrencyExchanger());
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onUpdateReceived(Update update) {

        Gson gson = new GsonBuilder().setPrettyPrinting().create(); // This is additional library to catch JSON type data
        URL url;
        ArrayList<CurrencyT> currencies = null; //We will get List of Object
        try {
            url = new URL("https://cbu.uz/oz/arkhiv-kursov-valyut/json/"); // This is Center Bank's API to get latest currency exchange rate
            URLConnection connection = url.openConnection(); // Here
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            Type type = new TypeToken<ArrayList<CurrencyT>>(){}.getType();
            currencies = gson.fromJson(reader, type);
        } catch (IOException e) {
            e.printStackTrace();
        }


        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());

        double resultCurrencyVal;

        if(update.getMessage().getText().toLowerCase(Locale.ROOT).equals("/start")){
            sendMessage.setText("Welcome to the Currency Exchanger Pro bot! \nIn this bot you can calculate your currency to different currency\nRight now \"" + update.getMessage().getChat().getUserName()+ "\" is using it!");
            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            sendMessage.setReplyMarkup(replyKeyboardMarkup);
            replyKeyboardMarkup.setResizeKeyboard(true);
            replyKeyboardMarkup.setSelective(true);
            replyKeyboardMarkup.setOneTimeKeyboard(true);

            List<KeyboardRow> keyboardRows  = new ArrayList<>();
            KeyboardRow row1 = new KeyboardRow();
            KeyboardButton button1 = new KeyboardButton("UZS->USD");
            KeyboardButton button2 = new KeyboardButton("UZS->EUR");
            KeyboardButton button3 = new KeyboardButton("UZS->CNY");
            row1.add(button1);
            row1.add(button2);
            row1.add(button3);
            KeyboardRow row2 = new KeyboardRow();
            KeyboardButton button4 = new KeyboardButton("USD->UZS");
            KeyboardButton button5 = new KeyboardButton("EUR->UZS");
            KeyboardButton button6 = new KeyboardButton("CNY->UZS");
            row2.add(button4);
            row2.add(button5);
            row2.add(button6);

            keyboardRows.add(row1);
            keyboardRows.add(row2);

            replyKeyboardMarkup.setKeyboard(keyboardRows);
        }
        else{

            if (!isNumeric(update.getMessage().getText())){
                if (update.getMessage().getText().equals("UZS->USD")) {
                    sendMessage.setText("Please input the value in UZS");
                    if (counterUZS_USD==0){
                        counterUZS_EUR=0;
                        counterUZS_CYN=0;
                        counterUSD_UZS=0;
                        counterEUR_UZS=0;
                        counterCYN_UZS=0;
                        ++counterUZS_USD;
                    }

                }
                else if (update.getMessage().getText().equals("UZS->EUR")) {
                    sendMessage.setText("Please input the value in UZS");
                    if (counterUZS_EUR==0){
                        counterUZS_USD=0;
                        counterUZS_CYN=0;
                        counterUSD_UZS=0;
                        counterEUR_UZS=0;
                        counterCYN_UZS=0;
                        ++counterUZS_EUR;
                    }
                }else if (update.getMessage().getText().equals("UZS->CNY")) {
                    sendMessage.setText("Please input the value in UZS");
                    if (counterUZS_CYN==0){
                        counterUZS_USD=0;
                        counterUZS_EUR=0;
                        counterUSD_UZS=0;
                        counterEUR_UZS=0;
                        counterCYN_UZS=0;
                        ++counterUZS_CYN;
                    }
                }else if (update.getMessage().getText().equals("USD->UZS")) {
                    sendMessage.setText("Please input the value in USD");
                    if (counterUSD_UZS==0){
                        counterUZS_USD=0;
                        counterUZS_EUR=0;
                        counterUZS_CYN=0;
                        counterEUR_UZS=0;
                        counterCYN_UZS=0;
                        ++counterUSD_UZS;
                    }
                }else if (update.getMessage().getText().equals("EUR->UZS")) {
                    sendMessage.setText("Please input the value in EUR");
                    if (counterEUR_UZS==0){
                        counterUZS_USD=0;
                        counterUZS_EUR=0;
                        counterUZS_CYN=0;
                        counterUSD_UZS=0;
                        counterCYN_UZS=0;
                        ++counterEUR_UZS;
                    }
                }else if (update.getMessage().getText().equals("CNY->UZS")) {
                    sendMessage.setText("Please input the value in CNY");
                    if (counterCYN_UZS==0){
                        counterUZS_USD=0;
                        counterUZS_EUR=0;
                        counterUZS_CYN=0;
                        counterUSD_UZS=0;
                        counterEUR_UZS=0;
                        ++counterCYN_UZS;
                    }
                }else {
                    sendMessage.setText("Please input value or select option!!!");
                }
            }
            else {
                if (counterUZS_USD !=0){
                    for (CurrencyT currency :currencies) {
                        if (currency.getCcy().equals("USD")){
                            resultCurrencyVal = Double.parseDouble(update.getMessage().getText()) / Double.parseDouble(currency.getRate());
                            sendMessage.setText(update.getMessage().getText() + " UZS  <===> " + resultCurrencyVal  + " USD " + " (Rate: " + currency.getRate() + ")");
                        }
                    }
                    counterUZS_USD--;

                }
                else if (counterUZS_EUR!=0){
                    for (CurrencyT currency :currencies) {
                        if (currency.getCcy().equals("EUR")){
                            resultCurrencyVal = Double.parseDouble(update.getMessage().getText()) / Double.parseDouble(currency.getRate());
                            sendMessage.setText(update.getMessage().getText() + " UZS  <===> " + resultCurrencyVal  + " EUR " + " (Rate: " + currency.getRate() + ")");
                        }
                    }
                    counterUZS_EUR--;

                }
                else if (counterUZS_CYN!=0){
                    for (CurrencyT currency :currencies) {
                        if (currency.getCcy().equals("CNY")){
                            resultCurrencyVal = Double.parseDouble(update.getMessage().getText()) / Double.parseDouble(currency.getRate());
                            sendMessage.setText(update.getMessage().getText() + " UZS  <===> " + resultCurrencyVal  + " CNY " + " (Rate: " + currency.getRate() + ")");
                        }
                    }
                    counterUZS_CYN--;

                }
                else if (counterUSD_UZS!=0){
                    for (CurrencyT currency :currencies) {
                        if (currency.getCcy().equals("USD")){
                            resultCurrencyVal = Double.parseDouble(update.getMessage().getText()) * Double.parseDouble(currency.getRate());
                            sendMessage.setText(update.getMessage().getText() + " USD  <===> " + resultCurrencyVal  + " UZS " + " (Rate: " + currency.getRate() + ")");
                        }
                    }
                    counterUSD_UZS--;

                }
                else if (counterEUR_UZS!=0){
                    for (CurrencyT currency :currencies) {
                        if (currency.getCcy().equals("EUR")){
                            resultCurrencyVal = Double.parseDouble(update.getMessage().getText()) * Double.parseDouble(currency.getRate());
                            sendMessage.setText(update.getMessage().getText() + " EUR  <===> " + resultCurrencyVal  + " UZS " + " (Rate: " + currency.getRate() + ")");
                        }
                    }
                    counterEUR_UZS--;

                }
                else if (counterCYN_UZS!=0){
                    for (CurrencyT currency :currencies) {
                        if (currency.getCcy().equals("CNY")){
                            resultCurrencyVal = Double.parseDouble(update.getMessage().getText()) * Double.parseDouble(currency.getRate());
                            sendMessage.setText(update.getMessage().getText() + " CNY  <===> " + resultCurrencyVal  + " UZS " + " (Rate: " + currency.getRate() + ")");
                        }
                    }
                    counterCYN_UZS--;

                }
                else {
                    sendMessage.setText("Please select the exchange option ");
                }
            }
        }

        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getBotUsername() {
        return "currency_exchanger_proBot";
    }

    @Override
    public String getBotToken() {
        return "2038871318:AAHUSvh8CTE5Sy1ed-L2qS4yUfuB23YHz_w";
    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

}